﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace store.Common
{
    public class CommonFunctions
    {

        public static void SendMail(MailMessage Mail)
        {
            try
            {
                /// Command line argument must the the SMTP host.
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("mallproject527@gmail.com", "mallproject");

                Mail.From = new MailAddress("mallproject527@gmail.com");
                Mail.BodyEncoding = UTF8Encoding.UTF8;
                Mail.IsBodyHtml = true;
                Mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                client.Send(Mail);
            }
            catch (Exception e)
            {

            }
        }
        public static DateTime GetCurrentDateTime()
        {
            DateTime todaysDate = DateTime.Now;
            var gmTime = todaysDate.ToUniversalTime();
            var indianTimeZone = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            var gmTimeConverted = TimeZoneInfo.ConvertTime(gmTime, indianTimeZone);
            DateTime ResultDateTime = Convert.ToDateTime(gmTimeConverted);
            return ResultDateTime;
        }

        public static int GetRandomNumber(int from, int To)

        {

            Random rnd = new Random();

            int RandomNumber = rnd.Next(from, To);

            return RandomNumber;

        }

        public static string RemoveSpecialCharacters(string illegal)
        {

            string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());

            foreach (char c in invalid)

            {

                illegal = illegal.Replace(c.ToString(), "");

            }

            return illegal;

        }

        public static string GetURLFriendlyString(string Value)

        {

            return Regex.Replace(Value, @"[^A-Za-z0-9_\~]+", "-");

        }

        public static string GetURLFriendlyStringWithDot(string Value)

        {

            return Regex.Replace(Value, @"[^A-Za-z0-9_\.~]+", "-");

        }


        public static string GetConnectionString()
        {
            // Put the name the Sqlconnection from WebConfig..
            //return @"Data Source=45.113.136.86\MSSQLSERVER2012;initial catalog=techxora_ECommerceEOrchid;persist security info=True;user id=techxora_sa;password=Techxorasa.;MultipleActiveResultSets=True;";

            return @"Data Source=TECHXORA-PC\SA;Initial Catalog=MallManagement;User ID=sa;Password=sa.;MultipleActiveResultSets=True;";

            //return @"Data Source=DESKTOP-0PQJ97U\SA;Initial Catalog=MallManagement;User ID=sa;Password=sa.;MultipleActiveResultSets=True;";

            // return @"Data Source=T1-08\SA;Initial Catalog=EmailExtractorUltimateF;User ID=sa;Password=sa.;MultipleActiveResultSets=True;";
        }

    }

    public static class StringExt
    {
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }

    static class Helper
    {
        public static string GetUntilOrEmpty(this string text, string stopAt = "-")
        {
            if (!String.IsNullOrWhiteSpace(text))
            {
                int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                if (charLocation > 0)
                {
                    return text.Substring(0, charLocation);
                }
                else
                {
                    return text;
                }
            }

            return String.Empty;
        }
    }
}