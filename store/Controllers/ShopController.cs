﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using store.Models;
using static store.Models.ViewModel;
using store.Common;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using System.IO;
using System.Collections.Generic;
using System.Net.Mail;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace store.Controllers
{
    public class ShopController : Controller
    {




        static string loc1 = "";

        static object lockObj = new object();//for remains value static ...
        MallManagementEntities db = new MallManagementEntities();

        #region Registration help files
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ShopController()
        {
        }

        public ShopController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";
        private string MallOwnerID;

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public object PageSize { get; private set; }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

        #region  Shop Owner registration
        public ActionResult ShopOwnerDetails()
        {
            //ViewBag.abc = "Registered Succesffully";
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ShopOwnerDetails(ShopOwnerViewModel DetailsPost, HttpPostedFileBase MyImage)
        {

            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = DetailsPost.EmailID, Email = DetailsPost.EmailID };
                var result = await UserManager.CreateAsync(user, DetailsPost.Password);
                if (result.Succeeded)
                {
                    UserManager.AddToRole(user.Id, "ShopOwner");


                    ShopOwnerDetailsTbl NewAdd = new ShopOwnerDetailsTbl();
                    NewAdd.ShopOwnerID = user.Id;
                    NewAdd.Name = DetailsPost.Name;
                    NewAdd.Address = DetailsPost.Address;
                    NewAdd.EmailID = DetailsPost.EmailID;
                    if (DetailsPost.DOB != null && DetailsPost.DOB != "")
                    {
                        var Date = DateTime.ParseExact(DetailsPost.DOB, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        NewAdd.DOB = Date;
                    }
                    NewAdd.Age = DetailsPost.Age;
                    NewAdd.PhnNo = DetailsPost.PhnNo;
                    NewAdd.Gender = DetailsPost.Gender;
                    db.ShopOwnerDetailsTbls.Add(NewAdd);
                    db.SaveChanges();
                    //for image upload
                    if (MyImage != null && MyImage.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(MyImage.FileName);

                        fileName = NewAdd.ShopOwnerID + "-ShopOwner-Image-" + fileName;

                        var path = Path.Combine(Server.MapPath("~/Uploads/ShopOwner"), fileName);

                        MyImage.SaveAs(path); //image store in server

                        NewAdd.img = fileName; //imgage path,db
                    }
                    db.SaveChanges();
                    //TempData["Message"] = "Registered Successfully!";
                    // ViewBag.abc = NewAdd.MallOwnerID;
                    return RedirectToAction("Login", "Account");
                }
                AddErrors(result);
            }

            return View(DetailsPost);
            // return View();

        }
        #endregion

        #region Edit Actions.....

        //For shop Edit

        public ActionResult ShopEdit(int ShopID)
        {
            //ViewBag.abc = "Registered Succesffully";
            ViewBag.ShopCategoriesvar = db.ShopCategoryTbls.ToList();
            var ShopOwnerID = User.Identity.GetUserId();
            var Shop = db.ShopTbls.Where(x => x.ShopOwnerID == ShopOwnerID && x.ShopID == ShopID).FirstOrDefault();
            ShopOwnerShopViewModel EditModel = new ShopOwnerShopViewModel();
            EditModel.ShopName = Shop.ShopName;
            EditModel.PhnNo = Shop.PhnNo;
            EditModel.LicenceNo = Shop.LicenceNo;
            EditModel.ShopCategoryID = Shop.ShopCategoryID;
            EditModel.ShopID = Shop.ShopID;
            EditModel.img = Shop.img;
            return View(EditModel);

        }

        [HttpPost]
        public ActionResult ShopEdit(ShopOwnerShopViewModel DetailsPost, HttpPostedFileBase MyImage)
        {

            if (ModelState.IsValid)
            {
                //ViewBag.abc = "Registered Succesffully";
                var GetShop = db.ShopTbls.Where(x => x.ShopID == DetailsPost.ShopID).FirstOrDefault();

                GetShop.ShopName = DetailsPost.ShopName;
                GetShop.PhnNo = DetailsPost.PhnNo;
                GetShop.LicenceNo = DetailsPost.LicenceNo;
                GetShop.ShopCategoryID = DetailsPost.ShopCategoryID;
                db.SaveChanges();
                if (MyImage != null && MyImage.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(MyImage.FileName);

                    fileName = DetailsPost.ShopID + "-ShopOwner-Image-" + fileName;

                    var path = Path.Combine(Server.MapPath("~/Uploads/ShopOwner"), fileName);

                    MyImage.SaveAs(path); //image store in server

                    GetShop.img = fileName; //imgage path,db
                }
                db.SaveChanges();
                return RedirectToAction("ShopView");
            }

            ViewBag.ShopCategoriesvar = db.ShopCategoryTbls.ToList();
            return View(DetailsPost);
            // return View();

        }

        //for shop owner profile edit.......
        [Authorize(Roles = "ShopOwner")]
        public ActionResult ShopOwnerProfileEdit()
        {
            string ShopOwnerID = User.Identity.GetUserId();


            var CheckDetails = db.ShopOwnerDetailsTbls.Where(x => x.ShopOwnerID == ShopOwnerID).FirstOrDefault();
            if (CheckDetails != null)
            {

                ShopOwnerViewModel EditShopOwner = new ShopOwnerViewModel();

                EditShopOwner.Name = CheckDetails.Name;
                EditShopOwner.Address = CheckDetails.Address;
                EditShopOwner.EmailID = CheckDetails.EmailID;
                EditShopOwner.DOB = Convert.ToDateTime(CheckDetails.DOB).ToString("dd-MM-yyyy");
                EditShopOwner.Age = CheckDetails.Age;
                EditShopOwner.Gender = CheckDetails.Gender;
                EditShopOwner.PhnNo = CheckDetails.PhnNo;
                EditShopOwner.img = CheckDetails.img;


                return View(EditShopOwner);
            }

            return View("ViewProfile");
        }

        [HttpPost]
        public ActionResult ShopOwnerProfileEdit(ShopOwnerViewModel PostModel, HttpPostedFileBase MyImage)
        {

            string ShopOwnerID = User.Identity.GetUserId();

            var ShopOwnerUpdate = db.ShopOwnerDetailsTbls.Where(x => x.ShopOwnerID == ShopOwnerID).FirstOrDefault();

            if (ShopOwnerUpdate != null)
            {
                ShopOwnerUpdate.Name = PostModel.Name;
                ShopOwnerUpdate.Address = PostModel.Address;
                ShopOwnerUpdate.EmailID = PostModel.EmailID;
                if (PostModel.DOB != null && PostModel.DOB != "")
                {
                    var Date = DateTime.ParseExact(PostModel.DOB, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ShopOwnerUpdate.DOB = Date;
                }
                ShopOwnerUpdate.Age = PostModel.Age;
                ShopOwnerUpdate.Gender = PostModel.Gender;
                ShopOwnerUpdate.PhnNo = PostModel.PhnNo;
                db.SaveChanges();
                //img upload-----------------------------------------------------
                if (MyImage != null && MyImage.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(MyImage.FileName);

                    fileName = ShopOwnerUpdate.ShopOwnerID + "-ShopOwner-Image-" + fileName;

                    var path = Path.Combine(Server.MapPath("~/Uploads/ShopOwner"), fileName);

                    MyImage.SaveAs(path); //image store in server

                    ShopOwnerUpdate.img = fileName; //imgage path,db
                }
                db.SaveChanges();
                //-------------------------------------------------------------------------------
                TempData["Message"] = "Saved Successfully!";
                return RedirectToAction("ViewProfile");

            }
            TempData["Message"] = "Record Not Found!";
            return View("ViewProfile");
        }

        //Mall owner Shop list edit(for editing floor no,room no,expry date etc....................)
        public ActionResult MallShopListEdit(int ShopID)
        {
            //ViewBag.abc = "Registered Succesffully"; 
            lock (lockObj)
            {
                loc1 = Request.UrlReferrer.ToString();//assigning url linked to this url
            }
            ViewBag.pre = Request.UrlReferrer.ToString();
            var Shop = db.ShopTbls.Where(x => x.ShopID == ShopID).FirstOrDefault();
            ShopViewModel EditModel = new ShopViewModel();
            EditModel.RoomNo = Shop.RoomNo;
            EditModel.FloorNo = Shop.FloorNo;
            EditModel.Amt = Shop.Amt;
            EditModel.DateOfExpiry = Shop.DateOfExpiry;
            return View(EditModel);

        }
        [HttpPost]
        public ActionResult MallShopListEdit(ShopViewModel DetailsPost)
        {
            if (ModelState.IsValid)
            {
                //ViewBag.abc = "Registered Succesffully";
                var GetShop = db.ShopTbls.Where(x => x.ShopID == DetailsPost.ShopID).FirstOrDefault();
                GetShop.RoomNo = DetailsPost.RoomNo;
                GetShop.FloorNo = DetailsPost.FloorNo;
                GetShop.Amt = DetailsPost.Amt;
                GetShop.DateOfExpiry = DetailsPost.DateOfExpiry;
                db.SaveChanges();
                ViewBag.loc1 = loc1;
                return Redirect(ViewBag.loc1);
            }
            return View(DetailsPost);
            // return View();
        }

        //Mall Owner RoomList Edit
        public ActionResult MallRoomListEdit(int ShopID)
        {
            //ViewBag.abc = "Registered Succesffully"; 
            lock (lockObj)
            {
                loc1 = Request.UrlReferrer.ToString();//assigning url linked to this url
            }
            ViewBag.pre = Request.UrlReferrer.ToString();
            var Shop = db.ShopTbls.Where(x => x.ShopID == ShopID).FirstOrDefault();
            ShopViewModel EditModel = new ShopViewModel();
            EditModel.RoomNo = Shop.RoomNo;
            EditModel.FloorNo = Shop.FloorNo;
            EditModel.SQFT = Shop.SQFT;
            EditModel.Amt = Shop.Amt;
            return View(EditModel);

        }
        [HttpPost]
        public ActionResult MallRoomListEdit(ShopViewModel DetailsPost)
        {
            if (ModelState.IsValid)
            {
                //ViewBag.abc = "Registered Succesffully";
                var GetShop = db.ShopTbls.Where(x => x.ShopID == DetailsPost.ShopID).FirstOrDefault();
                GetShop.RoomNo = DetailsPost.RoomNo;
                GetShop.FloorNo = DetailsPost.FloorNo;
                GetShop.SQFT = DetailsPost.SQFT;
                GetShop.Amt = DetailsPost.Amt;
                db.SaveChanges();
                ViewBag.loc1 = loc1;
                return Redirect(ViewBag.loc1);
            }
            return View(DetailsPost);
            // return View();
        }

        #endregion

        #region Add Rooms By Mall Owner.........
        // Mall Owner Shop(Room) Register part
        [Authorize(Roles = "MallOwner")]
        public ActionResult ShopRegMallOwner()
        {
            var MallOwnerID = User.Identity.GetUserId();

            var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();

            ViewBag.Mallvar = MallList;

            return View();
        }

        [Authorize(Roles = "MallOwner")]
        [HttpPost]
        public ActionResult ShopRegMallOwner(ShopViewModel DetailsPost)
        {

            if (ModelState.IsValid)
            {
                //ViewBag.abc = "Registered Succesffully";
                ShopTbl NewAdd = new ShopTbl();
                NewAdd.ShopID = DetailsPost.ShopID;

                // NewAdd.ShopOwnerID = DetailsPost.ShopOwnerID;

                NewAdd.MallID = DetailsPost.MallID;


                NewAdd.RoomNo = DetailsPost.RoomNo;
                NewAdd.FloorNo = DetailsPost.FloorNo;

                NewAdd.Amt = DetailsPost.Amt;
                NewAdd.SQFT = DetailsPost.SQFT;
                //  NewAdd.IsActive = DetailsPost.IsActive;
                // NewAdd.img = DetailsPost.img;

                db.ShopTbls.Add(NewAdd);
                db.SaveChanges();

                TempData["Message"] = "Added Successfully!";
                return RedirectToAction("ShopRegMallOwner");
            }
            var MallOwnerID = User.Identity.GetUserId();
            ViewBag.Mallvar = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            return View(DetailsPost);
        }
        #endregion

        #region View Region......

        public ActionResult MallShopUserListView()
        {

            var ShopOwnerDetailsViewForMall = db.ShopOwnerDetailsTbls.ToList();

            return View(ShopOwnerDetailsViewForMall);

        }

        #region MallOwner Shop view
        [Authorize(Roles = "MallOwner")]
        public ActionResult MallOwnerShopListViewSearch()
        {
            var MallOwnerID = User.Identity.GetUserId();
            var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            ViewBag.Mallvar = MallList;
            return View();
        }


        [HttpPost]
        public ActionResult MallOwnerShopListViewSearch(ComplaintBoxViewModel DetailsPost)
        {
            int MallID = DetailsPost.MallID;
            string ShopOrRooms = DetailsPost.ShopOrRooms;
            return RedirectToAction("MallOwnerShopListViewSearch", new { MallID = MallID, ShopOrRooms = ShopOrRooms });
        }
        #endregion

        [Authorize(Roles = "ShopOwner")]
        public ActionResult ViewProfile()
        {

            var ShopOwnerID = User.Identity.GetUserId();

            var ShopOwnerDetails = db.ShopOwnerDetailsTbls.Where(x => x.ShopOwnerID == ShopOwnerID).FirstOrDefault();

            return View(ShopOwnerDetails);
        }

        [Authorize(Roles = "ShopOwner")]
        public ActionResult ShopView()
        {

            var ShopOwnerID = User.Identity.GetUserId();

            var ShopList = db.ShopTbls.Where(x => x.ShopOwnerID == ShopOwnerID).ToList();

            return View(ShopList);
        }
        //Shop owner views shop details--------------------------------------------------------------------------------
        public ActionResult ShopViewDetails(int ShopID)
        {


            var ShopList = db.ShopTbls.Where(x => x.ShopID == ShopID).FirstOrDefault();

            return View(ShopList);

        }
        //Mall Owner View ShopOwner Shop based on his id--------------------------------------------------------------
        public ActionResult MallOwnerShopListViewOnID(string ShopOwnerID)
        {

            var MallOwnerShopListViewOnID = db.ShopTbls.Where(x => x.ShopOwnerID == ShopOwnerID).OrderByDescending(x => x.RegDate).ToList();

            //string pre2 = Request.UrlReferrer.ToString();//for geting url linked to this url
            return View(MallOwnerShopListViewOnID);
        }


        //Transaction View...................................................................................................
        public class ShopDetails
        {
            public int ShopID { get; set; }
            public string ShopName { get; set; }
        }

        public ActionResult ViewTransactionsSearch()
        {
            var ShopOwnerID = User.Identity.GetUserId();


            var parameter = new List<object>();
            var param = new SqlParameter("@ShopOwnerID", ShopOwnerID);
            parameter.Add(param);

            //param = new SqlParameter("@limit", limit);
            //parameter.Add(param);

            var ShopListDetails = db.Database.SqlQuery<ShopDetails>("EXEC GetShopListOfOwner @ShopOwnerID ", parameter.ToArray()).ToList();

            ViewBag.ShopList = ShopListDetails;


            //var Shop = db.ShopTbls.Where(x => x.ShopOwnerID == ShopOwnerID).ToList();

            //ViewBag.ShopVar = Shop;

            //var Model = new List<MallTbl>();
            //var MallList = db.MallTbls.ToList();
            //foreach (var id2 in MallList)
            //{
            //    foreach(var id in Shop)
            //    {
            //        if(id.MallID==id2.MallID)
            //        {
            //            Model.Add(id2);
            //        }
            //    }
            //}

            //ViewBag.Mallvar = Model.Distinct() ;
            return View();
        }
        [HttpPost]
        public ActionResult ViewTransactionsSearch(PaymentViewModel DetailsPost)
        {
            int? MallID = DetailsPost.MallID;
            int? ShopID = DetailsPost.ShopID;
            return RedirectToAction("ViewTransactionsSearch", new { MallID = MallID, ShopID = ShopID });
        }

        //Mall owner views transactions
        public ActionResult MallViewTransactionsSearch()
        {
            //var MallOwnerID = User.Identity.GetUserId();
            //var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            //ViewBag.Mallvar = MallList;
            //return View();

            var MallOwnerID = User.Identity.GetUserId();
            var ShopList = db.ShopTbls.ToList();
            var MallList = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            foreach (var id in MallList)
            {
                foreach (var id2 in ShopList)
                {
                    if (id.MallID == id2.MallID)
                    {

                        var abc = db.ShopTbls.Where(x => x.MallID == id2.MallID).OrderByDescending(x => x.RegDate).ToList();
                        ViewBag.ShopVar = abc;
                        ViewBag.MallVar = MallList;
                        return View();
                    }

                }

            }
            return View();

        }

        [HttpPost]
        public ActionResult MallViewTransactionsSearch(PaymentViewModel DetailsPost)
        {
            int? MallID = DetailsPost.MallID;
            int? ShopID = DetailsPost.ShopID;
            return RedirectToAction("MallViewTransactionsSearch", new { MallID = MallID, ShopID = ShopID });
        }

        //Mall Owner views Payment pending list
        public class PendingList
        {
            public int ShopID { get; set; }
            public string ShopName { get; set; }
            public decimal? PendingAmount { get; set; }
            public string MallName { get; set; }
            public string Name { get; set; }
            public string EmailID { get; set; }
        }

        public ActionResult PaymentPendingList()
        {
            var MallOwnerID = User.Identity.GetUserId();

            var parameter = new List<object>();
            var param = new SqlParameter("@MallOwnerID", MallOwnerID);
            parameter.Add(param);

            var ShopListDetails = db.Database.SqlQuery<PendingList>("EXEC pendingstatus @MallOwnerID ", parameter.ToArray()).ToList();

            ViewBag.ShopList = ShopListDetails;

            return View(ShopListDetails);
        }

        [HttpPost]
        public ActionResult PaymentPendingList(PaymentViewModel DetailsPost)
        {
            int? MallID = DetailsPost.MallID;
            int? ShopID = DetailsPost.ShopID;
            return RedirectToAction("PaymentPendingList", new { MallID = MallID, ShopID = ShopID });
        }

        #endregion

        #region Index Page Actions....
        //Mall Owner Page
        public ActionResult MallOwnerShop()
        {
            return View();

        }
        //ShopOwner Page
        [Authorize(Roles = "ShopOwner")]
        public ActionResult ShopOwnerPage()
        {
            return View();
        }

        //Shop owner shop View

        //payment
        [Authorize(Roles = "ShopOwner")]
        public ActionResult PaymentPage(int ShopID)
        {
            ViewBag.ShopID = ShopID;
            return View();
        }
        #endregion

        #region Payment actions.....
        //Debit card payment---------------------------------------------------------------------------------------------
        [Authorize(Roles = "ShopOwner")]
        public ActionResult DebitCardPayment(int ShopID)
        {
            var ShopOwnerID = User.Identity.GetUserId();


            //var Shop = db.ShopTbls.Where(x => x.ShopID == ShopID).FirstOrDefault();

            //var MallID = Shop.MallID;
            //var Mall = db.MallTbls.Where(x => x.MallID == MallID).ToList();
            //ViewBag.Mallvar = Mall;
            PaymentViewModel id = new PaymentViewModel();
            id.ShopID = ShopID;

            return View(id);
        }

        [HttpPost]
        public ActionResult DebitCardPayment(PaymentViewModel DetailsPost)
        {
            var Shop = db.ShopTbls.Where(x => x.ShopID == DetailsPost.ShopID).FirstOrDefault();
            int? MallID = Shop.MallID;
            decimal? Amt = DetailsPost.Amt;
            int? ShopID = DetailsPost.ShopID;
            return RedirectToAction("DebitCardPayment", new { MallID = MallID, Amt = Amt, ShopID = ShopID });
        }
        [Authorize(Roles = "ShopOwner")]
        [HttpPost]
        public ActionResult DebitCardPayment2(PaymentViewModel DetailsPost)
        {
            if (ModelState.IsValid)
            {
                MallShopTransactionTbl NewAdd = new MallShopTransactionTbl();
                NewAdd.ShopID = DetailsPost.ShopID;
                NewAdd.MallID = DetailsPost.MallID;

                NewAdd.Amt = DetailsPost.Amt;
                NewAdd.Date = DateTime.Now;

                db.MallShopTransactionTbls.Add(NewAdd);
                db.SaveChanges();
                TempData["Message"] = "Transaction Successfull!";


                return RedirectToAction("ShopView");
            }

            var Mall = db.MallTbls.ToList();
            return View(DetailsPost);
        }


        //credit card payment------------------------------------------------------------------------------
        [Authorize(Roles = "ShopOwner")]
        public ActionResult CreditCardPayment(int ShopID)
        {
            var ShopOwnerID = User.Identity.GetUserId();
            PaymentViewModel id = new PaymentViewModel();
            id.ShopID = ShopID;
            return View(id);
        }
        [HttpPost]
        public ActionResult CreditCardPayment(PaymentViewModel DetailsPost)
        {
            var Shop = db.ShopTbls.Where(x => x.ShopID == DetailsPost.ShopID).FirstOrDefault();
            int? MallID = Shop.MallID;
            decimal? Amt = DetailsPost.Amt;
            int? ShopID = DetailsPost.ShopID;
            return RedirectToAction("CreditCardPayment", new { MallID = MallID, Amt = Amt, ShopID = ShopID });
        }
        [Authorize(Roles = "ShopOwner")]
        [HttpPost]
        public ActionResult CreditCardPayment2(PaymentViewModel DetailsPost)
        {
            if (ModelState.IsValid)
            {
                MallShopTransactionTbl NewAdd = new MallShopTransactionTbl();
                NewAdd.ShopID = DetailsPost.ShopID;
                NewAdd.MallID = DetailsPost.MallID;

                NewAdd.Amt = DetailsPost.Amt;
                NewAdd.Date = DateTime.Now;

                db.MallShopTransactionTbls.Add(NewAdd);
                db.SaveChanges();
                TempData["Message"] = "Transaction Successfull!";


                return RedirectToAction("ShopView");
            }

            var Mall = db.MallTbls.ToList();
            return View(DetailsPost);
        }

        //Offline payment
        [Authorize(Roles = "ShopOwner")]
        public ActionResult OfflinePayment(int ShopID)
        {
            var ShopOwnerID = User.Identity.GetUserId();
            PaymentViewModel id = new PaymentViewModel();
            id.ShopID = ShopID;
            return View(id);
        }
        [Authorize(Roles = "ShopOwner")]
        [HttpPost]
        public ActionResult OfflinePayment(PaymentViewModel DetailsPost)
        {
            if (ModelState.IsValid)
            {
                MallShopTransactionTbl NewAdd = new MallShopTransactionTbl();
                NewAdd.ShopID = DetailsPost.ShopID;
                NewAdd.MallID = DetailsPost.MallID;

                NewAdd.Amt = DetailsPost.Amt;
                NewAdd.Date = DateTime.Now;

                db.MallShopTransactionTbls.Add(NewAdd);
                db.SaveChanges();
                TempData["Message"] = "Transaction Successfull!";


                return RedirectToAction("ShopView");
            }

            var Mall = db.MallTbls.ToList();
            return View(DetailsPost);
        }

        //Shop Owner gets payment slip-----------------------------------------------------------------------
        public ActionResult GetSlip(int MallShopTransactionID)
        {
            //var Shop = db.ShopTbls.Where(x => x.ShopID == ShopID).FirstOrDefault();
            var Shop = db.MallShopTransactionTbls.Where(x => x.MallShopTransactionID == MallShopTransactionID).FirstOrDefault();
            return View(Shop);
        }


        #endregion

        #region Interest Request Handling Actions
        public ActionResult ShopInterest()
        {
            var ShopList = db.ShopTbls.ToList();
            return View(ShopList);
        }

        // Shop Details
        public ActionResult ShopInterestDetails(int id)
        {
            var ShopOwnerID = User.Identity.GetUserId();
            bool IDExists = db.ShopInterestRequestTbls.Any(x => x.ShopID == id && x.ShopOwnerID==ShopOwnerID);

            if (IDExists)
            {
                TempData["Status"] = "<script>alert('Alredy Send');</script>";
                return RedirectToAction("ShopInterest");
            }
            else
            {
                var ShopDetail = db.ShopTbls.Where(x => x.ShopID == id).FirstOrDefault();

                ShopInterestViewModel ViewModel = new ShopInterestViewModel();
                ViewModel.ShopID = ShopDetail.ShopID;
                ViewModel.MallID = ShopDetail.MallID;
                ViewModel.RoomNo = ShopDetail.RoomNo;
                ViewModel.FloorNo = ShopDetail.FloorNo;
                ViewModel.IsActive = ShopDetail.IsActive;
                ViewModel.Amt = ShopDetail.Amt;

                return View(ViewModel);
            }
        }

        [HttpPost]
        public ActionResult ShopInterestDetails(ShopInterestViewModel DetailsPost)
        {
            var ShopOwnerID = User.Identity.GetUserId();

            ShopInterestRequestTbl NewAdd = new ShopInterestRequestTbl();
            NewAdd.ShopID = DetailsPost.ShopID;
            NewAdd.ShopOwnerID = ShopOwnerID;
            NewAdd.DateExpressed = DateTime.Now;
            NewAdd.Message = DetailsPost.Message;
            NewAdd.Status = true;
            db.ShopInterestRequestTbls.Add(NewAdd);
            db.SaveChanges();

            return RedirectToAction("ShopInterest");

        }

        //mall owner view shop interest
        public ActionResult MallOwnerShopInterest()
        {
            var MallOwnerID = User.Identity.GetUserId();
            var Model = new List<ShopInterestRequestTbl>();
            var ShopInterestList = db.ShopInterestRequestTbls.ToList();
            var Mall = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            var shop = db.ShopTbls.ToList();
            foreach (var id in Mall)
            {
                foreach (var id2 in shop)
                {
                    if (id.MallID == id2.MallID)
                    {
                        foreach (var id3 in ShopInterestList)
                        {
                            if (id3.ShopID == id2.ShopID)
                            {
                                //bool IDExists = db.ShopInterestRequestTbls.Any(x => x.ShopOwnerID == id3.ShopOwnerID);

                                if (id3.Status==true)
                                {
                                    Model.Add(id3);
                                }
                               
                            }
                        }
                    }
                }
            }

            // return View(Model.Distinct());
            return View(Model);
        }

        //Mall owner accept shop interest----------------------------------------------------------------------------
        public ActionResult InterestAccept(int ShopID, String ShopOwnerID)
        {
            ShopTbl obj = db.ShopTbls.Where(x => x.ShopID == ShopID).FirstOrDefault();

            if (obj.IsActive == true)
            {
                TempData["Status"] = "<script>alert('Alredy Exist');</script>";
            }
            else
            {
                ViewBag.Status = "No";
                obj.ShopOwnerID = ShopOwnerID;
                obj.RegDate = DateTime.Now;
                obj.IsActive = true;

                var p = db.ShopInterestRequestTbls.Where(x => x.ShopOwnerID == ShopOwnerID&& x.ShopID==ShopID).FirstOrDefault();
                db.ShopInterestRequestTbls.Remove(p);
                db.SaveChanges();


            }

            return RedirectToAction("MallOwnerShopInterest");
        }

        public ActionResult ShopInterestSenderDetails(String id)
        {
            var ShopInterester = db.ShopOwnerDetailsTbls.Where(x => x.ShopOwnerID == id).FirstOrDefault();
            return View(ShopInterester);
        }

        public ActionResult InterestReply(int ShopID,string ShopOwnerID)
        {
            // var ShopOwnerID = User.Identity.GetUserId();

            var Shop = db.ShopInterestRequestTbls.Where(x => x.ShopID == ShopID && x.ShopOwnerID==ShopOwnerID).ToList();
            ViewBag.ShopOwnerID = ShopOwnerID;
            ViewBag.ShopID = ShopID;
            return View(Shop);
        }
        [HttpPost]
        public ActionResult InterestReply(ShopInterestViewModel DetailsPost)
        {
            if (ModelState.IsValid)
            {

                ShopInterestRequestTbl NewAdd = new ShopInterestRequestTbl();
                NewAdd.ShopID = DetailsPost.ShopID;
                NewAdd.DateExpressed = DateTime.Now;
                NewAdd.ShopOwnerID = DetailsPost.ShopOwnerID;

                NewAdd.Reply = DetailsPost.Reply;
                db.ShopInterestRequestTbls.Add(NewAdd);
                db.SaveChanges();

                return RedirectToAction("InterestReply", new { ShopID = DetailsPost.ShopID , ShopOwnerID = DetailsPost.ShopOwnerID });
                //return View();
            }
            return View(DetailsPost);
        }

        //Shop Owner Reply To Mall Owner reply on shop interest
        public ActionResult ShopInterestReply(int ShopID)
        {
             var ShopOwnerID = User.Identity.GetUserId();
            var Shop = db.ShopInterestRequestTbls.Where(x => x.ShopID == ShopID && x.ShopOwnerID==ShopOwnerID).ToList();
            ViewBag.ShopID = ShopID;
            return View(Shop);
        }
        [HttpPost]
        public ActionResult ShopInterestReply(ShopInterestViewModel DetailsPost)
        {
            if (ModelState.IsValid)
            {

                ShopInterestRequestTbl NewAdd = new ShopInterestRequestTbl();
                var ShopOwnerID = User.Identity.GetUserId();
                NewAdd.ShopOwnerID = ShopOwnerID;
                NewAdd.ShopID = DetailsPost.ShopID;
                NewAdd.DateExpressed = DateTime.Now;
                NewAdd.Message = DetailsPost.Message;
                db.ShopInterestRequestTbls.Add(NewAdd);
                db.SaveChanges();

                return RedirectToAction("ShopInterestReply", new { ShopID = DetailsPost.ShopID });
                //return View();
            }
            return View(DetailsPost);
        }
        #endregion

        #region Remove Actions...............
        [HttpPost]
        public ActionResult RemoveShopMsg(int[] ids)
        {
            if (ids != null)
            {
                foreach (var id in ids)//checkbox checking
                {
                    ShopMallComplaintTbl NewAdd = db.ShopMallComplaintTbls.Find(id);
                    if (User.IsInRole("ShopOwner"))
                    {
                        NewAdd.ShopViewStatus = false;
                    }
                    else
                    {
                        NewAdd.MallViewStatus = false;
                    }

                    db.SaveChanges();

                    ShopMallComplaintTbl p = db.ShopMallComplaintTbls.Find(id);
                    if (NewAdd.MallViewStatus == NewAdd.ShopViewStatus)
                    {
                        db.ShopMallComplaintTbls.Remove(p);
                        db.SaveChanges();

                    }
                }
                TempData["Message"] = "Deleted Successfully!";
                //return RedirectToAction("MsgToMall", new { id = 0 });
            }
            return Redirect(Request.UrlReferrer.ToString());

        }
        //mall owner remove rooms----------------------------------------------------------------
        public ActionResult RemoveRoom(int ShopID)
        {
            ShopTbl p = db.ShopTbls.Find(ShopID);
            db.ShopTbls.Remove(p);
            db.SaveChanges();
            TempData["Message"] = "Deleted Successfully!";
            return Redirect(Request.UrlReferrer.ToString());

            //return RedirectToAction("MallOwnerMallList", new { MallID = 0 });
        }

        //Mall owner remove shops----------------------------------------------------------------------------------------
        public ActionResult RemoveShop(int ShopID)
        {
            ShopTbl p = db.ShopTbls.Find(ShopID);
            db.ShopTbls.Remove(p);
            db.SaveChanges();
            TempData["Message"] = "Deleted Successfully!";
            return Redirect(Request.UrlReferrer.ToString());
        }

        //Mall owner removes user profiles
        public ActionResult RemoveUser(string ShopOwnerID)
        {
            ShopOwnerDetailsTbl p1 = db.ShopOwnerDetailsTbls.Where(x => x.ShopOwnerID == ShopOwnerID).FirstOrDefault();
            db.ShopOwnerDetailsTbls.Remove(p1);
            db.SaveChanges();
            TempData["Message"] = "Deleted Successfully!";
            // return Redirect(Request.UrlReferrer.ToString());
            return RedirectToAction("MallShopUserListView");
        }

        //Mall owner removes discontinue shops
        public ActionResult RemoveShopDiscontinue(int ShopID)
        {
            RemoveShopTbl p1 = db.RemoveShopTbls.Where(x => x.ShopID == ShopID).FirstOrDefault();
            db.RemoveShopTbls.Remove(p1);
            db.SaveChanges();
            TempData["Message"] = "Deleted Successfully!";
            // return Redirect(Request.UrlReferrer.ToString());
            return RedirectToAction("MallViewShopDiscontinue");
        }

        //Shop Owner delete transaction
        [HttpPost]
        public ActionResult RemoveTransaction(int[] ids)
        {
            if (ids != null)
            {
                foreach (var id in ids)
                {
                    MallShopTransactionTbl NewAdd = db.MallShopTransactionTbls.Find(id);
                    if (User.IsInRole("MallOwner"))
                    {
                        NewAdd.MallViewStatus = false;
                    }
                    else
                    {
                        NewAdd.ShopViewStatus = false;
                    }
                    db.SaveChanges();

                    MallShopTransactionTbl p = db.MallShopTransactionTbls.Find(id);
                    if (NewAdd.MallViewStatus == NewAdd.ShopViewStatus)
                    {
                        db.MallShopTransactionTbls.Remove(p);
                        db.SaveChanges();
                    }
                }
                TempData["Message"] = "Deleted Successfully!";
                //return RedirectToAction("ViewOwnerEmpPay");
            }

            return Redirect(Request.UrlReferrer.ToString());
        }


        #endregion

        #region Shop Discontinue Handling......................................
        //Discontinue request Added to table
        public ActionResult ShopDiscontinue(int ShopID)
        {
            var ShopOwnerID = User.Identity.GetUserId();
            bool IDExists = db.RemoveShopTbls.Any(x => x.ShopID == ShopID && x.ShopOwnerID==ShopOwnerID);
         
            if (IDExists)
            {
                TempData["Message"] = "Already Send!";
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
            {
               
                var Shop = db.ShopTbls.Where(x => x.ShopID == ShopID).FirstOrDefault();
                RemoveShopTbl NewAdd = new RemoveShopTbl();
                NewAdd.ShopID = ShopID;
                NewAdd.ShopName = Shop.ShopName;
                NewAdd.MallID = Shop.MallID;
                NewAdd.PhnNo = Shop.PhnNo;
                NewAdd.RoomNo = Shop.RoomNo;
                NewAdd.FloorNo = Shop.FloorNo;
                NewAdd.LicenceNo = Shop.LicenceNo;
                NewAdd.ShopOwnerID = Shop.ShopOwnerID;
                NewAdd.DateOfExpiry = Shop.DateOfExpiry;
                NewAdd.ShopCategoryID = Shop.ShopCategoryID;
                NewAdd.Amt = Shop.Amt;
                NewAdd.RegDate = Shop.RegDate;
                //NewAdd.TotalAmtPaid = Shop.TotalAmtPaid;

                //NewAdd.TotalDays = ShopDetail.TotalDays;
                //NewAdd.WithdrawDate = ShopDetail.WithdrawDate;
                db.RemoveShopTbls.Add(NewAdd);
                db.SaveChanges();
                TempData["Message"] = "Send Successfully!";
                return Redirect(Request.UrlReferrer.ToString());
            }
        }

        //RemoveShop Tbl view for Mall Owner
        public ActionResult MallViewShopDiscontinue()
        {
            var MallOwnerID = User.Identity.GetUserId();
            var Mall = db.MallTbls.Where(x => x.MallOwnerID == MallOwnerID).ToList();
            var Discontinue = db.RemoveShopTbls.ToList();
            var Model = new List<RemoveShopTbl>();
            foreach (var id2 in Mall)
            {
                foreach (var id in Discontinue)
                {
                    if (id.MallID == id2.MallID)
                    {
                        Model.Add(id);
                    }
                }
            }
            return View(Model);
        }

        //Mall Owner Discontinue Request Accept Action...............
        public ActionResult MallAcceptShopDiscontinue(int ShopID)
        {

            var RemoveShop = db.RemoveShopTbls.Where(x => x.ShopID == ShopID).FirstOrDefault();
            RemoveShop.WithdrawDate = DateTime.Now;
            RemoveShop.Status = true;
            //for Total day calculation.....
            DateTime RegDate = Convert.ToDateTime(RemoveShop.RegDate);
            DateTime WithdrawDate = Convert.ToDateTime(RemoveShop.WithdrawDate);
            int TotalDays = WithdrawDate.Day - RegDate.Day;
            RemoveShop.TotalDays = TotalDays;
            db.SaveChanges();

            var Shop = db.ShopTbls.Where(x => x.ShopID == ShopID).FirstOrDefault();
            Shop.ShopName = null;
            Shop.PhnNo = null;
            Shop.LicenceNo = null;
            Shop.ShopOwnerID = null;
            Shop.DateOfExpiry = null;
            Shop.ShopCategoryID = null;
            Shop.RegDate = null;
            Shop.IsActive = null;

            db.SaveChanges();
            //Email alert to shop interest senders---------------------------------------
            var p = db.ShopInterestRequestTbls.Where(x =>x.ShopID == ShopID).ToList();
            var shop = db.ShopTbls.Where(x => x.ShopID == ShopID).FirstOrDefault();
            var mall = db.MallTbls.Where(x => x.MallID == shop.MallID).FirstOrDefault();
            var p1 = db.ShopOwnerDetailsTbls.ToList();
            try { 
            foreach (var Owner1 in p)
            {
                foreach (var Owner2 in p1 )
                {
                    if (Owner2.ShopOwnerID == Owner1.ShopOwnerID)
                    {
                        MailMessage Mail = new MailMessage();
                        Mail.To.Add(Owner2.EmailID);

                        Mail.Subject = "Your Room is available";

                        string Body = "Hello " + Owner2.Name;
                        Body += "<hr>You have Requested room in our mall" + mall.MallName + "For your shop" +shop.RoomNo;
                        Body += "<hr>Contact us for further details";
                        Mail.Body = Body;
                        CommonFunctions.SendMail(Mail);
                    }
                }
                
            }
            }
            catch(Exception e)
            { }

            TempData["Message"] = "Removed Successfully!";
            return Redirect(Request.UrlReferrer.ToString());
        }

        //Mall owner view discontinue shops details------------------------------------------------------------------------------
        public ActionResult ShopDiscontinueDetails(int ShopID)
        {
            var ShopDetails = db.RemoveShopTbls.Where(x => x.ShopID == ShopID).FirstOrDefault();
            return View(ShopDetails);
        }
        #endregion

        #region Messages/Complaints
        // Complaints
        public ActionResult MallMsgInbox()
        {
            var MallOwnerID = User.Identity.GetUserId();
            //var Shop = db.ShopTbls.Where(x => x.MallOwnerID == ShopID).ToList();
            return View(MallOwnerID);
        }
        public ActionResult SendShopMallMsg()
        {
            var ShopOwnerID = User.Identity.GetUserId();

            var parameter = new List<object>();
            var param = new SqlParameter("@ShopOwnerID", ShopOwnerID);
            parameter.Add(param);
            //param = new SqlParameter("@limit", limit);
            //parameter.Add(param);
            var ShopListDetails = db.Database.SqlQuery<ShopDetails>("EXEC GetShopListOfOwner @ShopOwnerID ", parameter.ToArray()).ToList();

            ViewBag.ShopVar = ShopListDetails;
            //var Shop = db.ShopTbls.Where(x => x.ShopOwnerID == ShopOwnerID).ToList();
            //ViewBag.ShopVar = Shop;
            return View();
        }
        [HttpPost]
        public ActionResult SendShopMallMsg2(ComplaintBoxViewModel DetailsPost)
        {
            ShopMallComplaintTbl NewAdd = new ShopMallComplaintTbl();
            NewAdd.ShopID = DetailsPost.ShopID;
            NewAdd.WhoStatus = false;
            NewAdd.DatePosted = DateTime.Now;
            NewAdd.Msg = DetailsPost.Msg;
            db.ShopMallComplaintTbls.Add(NewAdd);
            db.SaveChanges();
            return RedirectToAction("MsgToMall");
        }

        public ActionResult MallShopSendMsg(int ShopID)
        {
            ComplaintBoxViewModel id = new ComplaintBoxViewModel();
            id.ShopID = ShopID;
            //var Shop = db.ShopMallComplaintTbls.Where(x => x.ShopID == ShopID).FirstOrDefault();
            return View(id);
        }


        [HttpPost]
        public ActionResult MallShopSendMsg(ComplaintBoxViewModel DetailsPost)
        {
            ShopMallComplaintTbl NewAdd = new ShopMallComplaintTbl();
            NewAdd.ShopID = DetailsPost.ShopID;
            NewAdd.WhoStatus = true;
            NewAdd.DatePosted = DateTime.Now;
            NewAdd.Msg = DetailsPost.Msg;
            db.ShopMallComplaintTbls.Add(NewAdd);
            db.SaveChanges();
            return RedirectToAction("MallOwnerShopListViewSearch");
        }

        //Mall Owner sends message to shop owner
        public ActionResult MsgToShop()
        {
            return View();
        }

        //Shop Owner sends Message to Mall Owner
        public ActionResult MsgToMall()
        {
            return View();
        }

        //mall owner reply to shop owner message
        public ActionResult ShopMallSendMsg(int ShopID)
        {
            ComplaintBoxViewModel id = new ComplaintBoxViewModel();
            id.ShopID = ShopID;
            return View(id);
        }

        [HttpPost]
        public ActionResult ShopMallSendMsg(ComplaintBoxViewModel DetailsPost)
        {
            ShopMallComplaintTbl NewAdd = new ShopMallComplaintTbl();
            NewAdd.ShopID = DetailsPost.ShopID;
            NewAdd.WhoStatus = true;
            NewAdd.DatePosted = DateTime.Now;
            NewAdd.Msg = DetailsPost.Msg;
            db.ShopMallComplaintTbls.Add(NewAdd);
            db.SaveChanges();
            return RedirectToAction("MsgToShop");
        }

        //Shop Owner reply to mall owner message
        public ActionResult ShopOwnerToMallSendMsg(int ShopID)
        {
            ComplaintBoxViewModel id = new ComplaintBoxViewModel();
            id.ShopID = ShopID;
            return View(id);
        }

        [HttpPost]
        public ActionResult ShopOwnerToMallSendMsg(ComplaintBoxViewModel DetailsPost)
        {
            ShopMallComplaintTbl NewAdd = new ShopMallComplaintTbl();
            NewAdd.ShopID = DetailsPost.ShopID;
            NewAdd.WhoStatus = false;
            NewAdd.DatePosted = DateTime.Now;
            NewAdd.Msg = DetailsPost.Msg;
            db.ShopMallComplaintTbls.Add(NewAdd);
            db.SaveChanges();
            return RedirectToAction("MsgToMall");
        }
        #endregion

        #region Send Generated Emails..............................

        // Mall Owner send rent payement alert emails to shop owner...........................................
        public ActionResult SendMail()
        {
            //List<string> Mails = new List<string>();

            //Mails.Add("dhanil.vd@gmail.com");


            List<string> Mails = new List<string>();

            var MallOwnerID = User.Identity.GetUserId();

            var parameter = new List<object>();
            var param = new SqlParameter("@MallOwnerID", MallOwnerID);
            parameter.Add(param);

            var ShopListDetails = db.Database.SqlQuery<PendingList>("EXEC pendingstatus @MallOwnerID ", parameter.ToArray()).ToList();

            //ViewBag.ShopList = ShopListDetails;

            //return View(ShopListDetails);


            foreach (var owner in ShopListDetails)
            {
                MailMessage Mail = new MailMessage();
                Mail.To.Add(owner.EmailID);

                Mail.Subject = "Rent Payment Pending";

                string Body = "Hello " + owner.Name;
                Body += "<hr>You have pending amount of rupees" + owner.PendingAmount + "For your shop" + owner.ShopName;
                Body += "<hr>So Kindly pay";
                Mail.Body = Body;
                CommonFunctions.SendMail(Mail);
            }


            return RedirectToAction("PaymentPendingList");
        }

        //Mall owner send mail to interest senders when a shop discontinued
        public ActionResult SendMailToInterester()
        {

            return View();
        }

        #endregion

        #region Reports.................
        public ActionResult MallReportsPage()
        {
            return View();
        }
        public ActionResult RegShopsReport(int MallID)
        {
            return View();
        }
        public ActionResult RegUsersReport(int MallID)
        {
            return View();
        }
        public ActionResult TransactionsReport()
        {
            return View();
        }
        #endregion


        #region For PDF Print............

        public void DownloadPDF(int MallShopTransactionID)
        {


            var Bill = db.MallShopTransactionTbls.Where(x => x.MallShopTransactionID == MallShopTransactionID).FirstOrDefault();
            var ShopList = db.ShopTbls.ToList();


            string html = "";
            html += "<h1 align=center>PAYMENT RECEIPT</h1>";
            html += "<---------------------------------------------------------------------------------------------------------------------------------------";
            html += "<br><br><br><br><b> Transaction ID:" + Bill.MallShopTransactionID + "</b><br>";
            html += "<b>  Date:" + "</b>" + Bill.Date + "<br>";
            foreach (var id in ShopList)
            {
                if (Bill.ShopID == id.ShopID)
                {


                    html += "<b>  Mobile:" + "</b>" + id.PhnNo + "<br>";

                }
            }

            foreach (var id in ShopList)
            {
                if (Bill.ShopID == id.ShopID)
                {
                    html += "<b> Mall Name:" + "</b>" + id.ShopName + "</br>";
                }
            }
            html += "</br>---------------------------------------------------------------------------------------------------------------------------------------";
            html += "<br><br><br><br><br><br><table><b><tr><td>DESCRIPTION</td><td>AMOUNT</b></td></tr>";
            html += "<tr><td>" + "Payment For" + Bill.Date + "</td><td>" + Bill.Amt + "</td><td>";


            html += "<tr><td></td><td></td><td><b>Grand Total</b></td><td>" + Bill.Amt + "</td></tr></table>";
            string HTMLContent = html;
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + "PDFfile.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(GetPDF(HTMLContent));
            Response.End();
        }

        public byte[] GetPDF(string pHTML)
        {
            byte[] bPDF = null;

            MemoryStream ms = new MemoryStream();
            TextReader txtReader = new StringReader(pHTML);

            // 1: create object of a itextsharp document class  
            Document doc = new Document();

            // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file  
            PdfWriter oPdfWriter = PdfWriter.GetInstance(doc, ms);

            // 3: we create a worker parse the document  
            HTMLWorker htmlWorker = new HTMLWorker(doc);

            // 4: we open document and start the worker on the document  
            doc.Open();
            htmlWorker.StartDocument();


            // 5: parse the html into the document  
            htmlWorker.Parse(txtReader);

            // 6: close the document and the worker  
            htmlWorker.EndDocument();
            htmlWorker.Close();
            doc.Close();

            bPDF = ms.ToArray();

            return bPDF;
        }
        #endregion
    }
}
