//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace store.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MallEmpPayTbl
    {
        public int MallEmpPayID { get; set; }
        public string MallEmpID { get; set; }
        public Nullable<decimal> AmountPaid { get; set; }
        public Nullable<System.DateTime> DatePaid { get; set; }
        public Nullable<int> MallID { get; set; }
    }
}
