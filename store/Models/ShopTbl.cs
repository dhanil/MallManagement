//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace store.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ShopTbl
    {
        public int ShopID { get; set; }
        public int MallID { get; set; }
        public string ShopName { get; set; }
        public string PhnNo { get; set; }
        public Nullable<int> RoomNo { get; set; }
        public Nullable<int> FloorNo { get; set; }
        public string LicenceNo { get; set; }
        public string ShopOwnerID { get; set; }
        public Nullable<System.DateTime> DateOfExpiry { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> ShopCategoryID { get; set; }
        public Nullable<decimal> Amt { get; set; }
        public string img { get; set; }
        public Nullable<System.DateTime> RegDate { get; set; }
        public Nullable<decimal> SQFT { get; set; }
    }
}
