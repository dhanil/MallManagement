﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using store.Models;


namespace store.Models
{
    public class ViewModel
    {
        public class ShopOwnerViewModel
        {
            public int ShopOwnerDetailsID { get; set; }
            [Display(Name = "Shop OwnerID")]
            public String ShopOwnerID { get; set; }

            [Required]
            [Display(Name = "Name")]
            public string Name { get; set; }

            [Required]
            [Display(Name = "Address")]
            public string Address { get; set; }

            [Required]
            [EmailAddress]
            [Display(Name = "Email ID")]
            public string EmailID { get; set; }

            [Required]
            [Display(Name = "Date of Birth")]
            public string DOB { get; set; }

            [Required]
            [Display(Name = "Age")]
            public Nullable<int> Age { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 10)]
            [Display(Name = "Phone.No")]
            public string PhnNo { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            public string Gender { get; set; }

            [Display(Name = "Image")]
            public string img { get; set; }

        }

        public class ShopOwnerLoginViewModel
        {
            [Required]
            [Display(Name = "User Name")]
            public string UserID { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [Display(Name = "Remember me?")]
            public bool RememberMe { get; set; }
        }


        public class ShopOwnerShopViewModel
        {
            [Display(Name = "Shop ID")]
            public int ShopID { get; set; }

            [Display(Name = "Registered date")]
            public Nullable<System.DateTime> RegDate { get; set; }

            //[Required]
            [Display(Name = "Shop Name")]
            public string ShopName { get; set; }

           // [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 10)]
            [Display(Name = "Phone.No")]
            public string PhnNo { get; set; }


            //[Required]
            [Display(Name = "Shop Category")]
            public int? ShopCategoryID { get; set; }


           // [Required]
            [Display(Name = "Licence Number")]
            public string LicenceNo { get; set; }

            //  [Required]
            //  [Display(Name = "Is Active")]
            //  public bool? IsActive { get; set; }
            //  public Nullable<bool> IsActive { get; set; }
            [Display(Name = "Image")]
            public string img { get; set; }
        }

        public class ShopInterestViewModel
        {
            [Display(Name = "Shop ID")]
            public int ShopID { get; set; }

            [Display(Name = "Mall ID")]
            public int MallID { get; set; }

            [Display(Name = "Mall Name")]
            public int MallName { get; set; }

            [Display(Name = "Room Number")]
            public int? RoomNo { get; set; }

            [Display(Name = "Floor Number")]
            public int? FloorNo { get; set; }

            //[Required]
            //[Display(Name = "Date Of Expiry")]
            //public DateTime DateOfExpiry { get; set; }

            //  [Required]
            [Display(Name = "Is Active")]
            public bool? IsActive { get; set; }



            // for shop interest request using the same model (Shop interest details)

            [Display(Name = "Shop Interest Request ID")]
            public int ShopInterestRequestID { get; set; }

            [Display(Name = "Shop Owner ID")]
            public string ShopOwnerID { get; set; }

            [Display(Name = "Shop Owner Name")]
            public string ShopOwnerName { get; set; }

            [Display(Name = "Date Expressed")]
            public DateTime DateExpressed { get; set; }

            [Display(Name = "Message")]
            public string Message { get; set; }

            [Display(Name ="Reply")]
            public string Reply { get; set; }

            [Display(Name = "Amount")]
            public decimal? Amt { get; set; }
            [Display(Name = "Square Feet")]
            public decimal? SQFT { get; set; }
        }

        public class ShopViewModel
        {

            [Display(Name = "Shop ID")]
            public int ShopID { get; set; }

            [Required]
            [Display(Name = "Mall Name")]
            public int MallID { get; set; }

            [Required]
            [Display(Name = "Room Number")]
            public Nullable<int> RoomNo { get; set; }
            [Display(Name = "Registered date")]
            public Nullable<System.DateTime> RegDate { get; set; }
            [Required]
            [Display(Name = "Floor Number")]
            public Nullable<int> FloorNo { get; set; }


            //[Required]
            [Display(Name = "Date Of Expiry")]
            public Nullable<System.DateTime> DateOfExpiry { get; set; }

            //  [Required]
            [Display(Name = "Is Active")]
            public bool? IsActive { get; set; }
            [Required]
            [Display(Name = "Amount")]
            public decimal? Amt { get; set; }
            [Required]
            [Display(Name = "Square Feet")]
            public decimal? SQFT { get; set; }
        }

     
        //Payment
        public class PaymentViewModel
        {
            [Required]
            [Display(Name ="Shop")]
            public Nullable<int> ShopID { get; set; }

            [Display(Name = "Shop Name")]
            public string ShopName { get; set; }

            [Display(Name = "Amount")]
            public decimal? Amt { get; set; }

            [Display(Name = "Date")]
            public DateTime Date { get; set; }

            [Display(Name = "Mall Owner Name")]
            public string MallOwnerID { get; set; }

            [Display(Name = "Mall")]
           public Nullable<int> MallID { get; set; }

            [Display(Name = "Mall Name")]
            public Nullable<int> MallName { get; set; }
        }


        //Complaint model
        public class ComplaintBoxViewModel
        {
            public int ShopMallComplaintID { get; set; }
            public Nullable<int> ShopID { get; set; }
            public Nullable<bool> WhoStatus { get; set; }
            public Nullable<System.DateTime> DatePosted { get; set; }
            public string Msg { get; set; }
            public Nullable<bool> MallViewStatus { get; set; }
            public Nullable<bool> ShopViewStatus { get; set; }

            [Display(Name ="MallOWnerID")]
            public string MallOwnerID { get; set; }

            [Display(Name = "Mall ID")]
            public int MallID { get; set; }

            [Display(Name = "Mall Name")]
            public int MallName { get; set; }

            [Display(Name = "Shop Owner ID")]
            public int ShopOwnerID { get; set; }

            [Display(Name = "Choose Any:")]
            public string ShopOrRooms { get; set; }
        }
        public class RemoveShopViewModel
        {
            public int RemoveShopID { get; set; }
            [Display(Name = "Shop ID")]
            public Nullable<int> ShopID { get; set; }
            [Display(Name = "Mall")]
            public Nullable<int> MallID { get; set; }
            [Display(Name = "Phone No")]
            public string PhnNo { get; set; }
            [Display(Name = "Room No")]
            public Nullable<int> RoomNo { get; set; }
            [Display(Name = "Floor No")]
            public Nullable<int> FloorNo { get; set; }
            [Display(Name = "Licence No")]
            public string LicenceNo { get; set; }
            [Display(Name = "Shop Owner ID")]
            public string ShopOwnerID { get; set; }
            [Display(Name = "DateOfExpiry")]
            public Nullable<System.DateTime> DateOfExpiry { get; set; }
            [Display(Name = "Status")]
            public Nullable<bool> Status { get; set; }
            [Display(Name = "ShopCategoryID")]
            public Nullable<int> ShopCategoryID { get; set; }
            [Display(Name = "Amount")]
            public Nullable<decimal> Amt { get; set; }
            [Display(Name = "Square Feet")]
            public decimal? SQFT { get; set; }
            [Display(Name = "Total Amount Paid")]
            public Nullable<decimal> TotalAmtPaid { get; set; }
            [Display(Name = "Total Days")]
            public Nullable<int> TotalDays { get; set; }
            [Display(Name = "Withdraw Date")]
            public Nullable<System.DateTime> WithdrawDate { get; set; }
            [Display(Name = "Registered date")]
            public Nullable<System.DateTime> RegDate { get; set; }
        }

    }
}